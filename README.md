# Utilizando o box

O box é uma das principais ferramentas utilizadas para criar problemas na América Latina. O seu repositório [pode ser encontrado no GitLab](https://gitlab.com/icpc-la/tools/box).
Aqui, apenas clonamos o box e mudamos o padrão de inglês para português nas definições do TeX.

Para utilizar o box, clone este repositório e faça `. env.sh` ou `source env.sh`. Assim, na sua sessão de bash atual, você poderá utilizar o comando `box` para criar problemas no diretório atual.

O manual completo do box pode ser encontrado [dentro do diretório do box](.box/box-manual.md).
